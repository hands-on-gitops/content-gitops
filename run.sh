fluxctl version
kubectl get nodes
kubectl get pods --all-namespaces
kubectl create namespace flux
export GLUSER=811215-guilhermerodrigues@users.noreply.gitlab.com
env | grep GL

fluxctl install --git-user=${GLUSER} \
--git-email=${GLUSER}@gmail.com \
--git-url=git@gitlab.com:${GLUSER}/content-gitops \
--git-path=namespaces,workloads \
--namespace=flux | kubectl apply -f -

kubectl -n flux rollout status deployment/flux

fluxctl identity --k8s-fwd-ns flux

fluxctl sync --k8s-fwd-ns flux

kubectl get pods --all-namespaces
